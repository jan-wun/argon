from django.db import models


class InstagramPicture(models.Model):
    verbose_name = "Instagram-Bild"

    # instagram_user = models.ForeignKey(...)
    key = models.CharField(max_length=50, null=False)
    number_of_likes = models.IntegerField(null=True)
    # Cw5YNpxLWKc


    # - InstagramPictures(InstagramUser_ProfilName, Key, Anzahl_Likes)